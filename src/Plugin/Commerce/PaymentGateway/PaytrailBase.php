<?php

declare(strict_types = 1);

namespace Drupal\commerce_paytrail\Plugin\Commerce\PaymentGateway;

/**
 * Provides the Paytrail payment gateway.
 *
 * @deprecated in commerce_paytrail:2.5.0 and is removed from commerce_paytrail:3.0.0.
 * Use \Drupal\commerce_paytrail\Commerce\PaymentGateway\Paytrail instead.
 *
 * @see https://www.drupal.org/project/commerce_paytrail/issues/3181551
 */
class PaytrailBase extends Paytrail {
}
