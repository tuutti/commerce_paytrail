<?php

declare(strict_types = 1);

namespace Drupal\commerce_paytrail\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the paytrail payment method type method type.
 *
 * @deprecated in commerce_paytrail:2.5.0 and is removed from commerce_paytrail:3.0.0.
 * This was accidentally added to initial 8.x release and was actually never
 * used.
 *
 * @see https://www.drupal.org/project/commerce_paytrail/issues/3183954
 *
 * @CommercePaymentMethodType(
 *   id = "paytrail",
 *   label = @Translation("Paytrail"),
 *   create_label = @Translation("Paytrail"),
 * )
 */
class Paytrail extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->t('Paytrail');
  }

}
