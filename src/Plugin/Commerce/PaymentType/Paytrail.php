<?php

declare(strict_types = 1);

namespace Drupal\commerce_paytrail\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentDefault;

/**
 * Provides the default payment type.
 *
 * @deprecated in commerce_paytrail:2.5.0 and is removed from commerce_paytrail:3.0.0.
 * This was accidentally added to initial 8.x release and was actually never
 * used.
 *
 * @see https://www.drupal.org/project/commerce_paytrail/issues/3183954
 *
 * @CommercePaymentType(
 *   id = "paytrail",
 *   label = @Translation("Paytrail"),
 * )
 */
class Paytrail extends PaymentDefault {}
